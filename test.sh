#!/bin/bash

# test.sh
# Tests the totalcount package

# Author: Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)
# URL:    https://gitlab.com/axelsommerfeldt/caption
# Date:   2020-12-16

source ./test-lib.sh

disable unsorted  # TODO

main "$@"


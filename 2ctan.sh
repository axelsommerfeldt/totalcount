#!/bin/sh
#
# 2011-10-09: 1st version
# 2011-10-10: Log file will be copied as ".LOG" now (needs package "mmv")
# 2011-11-06: File CHANGELOG added to distribution
# 2012-03-15: Option "-p" added to "cp" command
# 2013-01-08: "-U dante" changed to "-u http://www.ctan.org/upload"
# 2013-02-03: Adapted to new SVN directory structure
# 2015-09-17: Revised, ctanupload commented out since currently-not-working
# 2020-08-01: Adapted to the "totalcount" package
# 2020-08-30: Option "--notds" added to call of ctanify, as requested by Petra Rübe-Pugliese (CTAN Team)
#
# Needs on CentOS/Fedora: mmv perl-File-Copy-Recursive perl-HTML-FormatText* perl-WWW-Mechanize* perl-XML-TreeBuilder
#
set -e
dist_dir=$(pwd)
temp_dir="/tmp/totalcount"

rm -fr "$temp_dir"
mkdir "$temp_dir"
cp -a source/*.ins source/*.dtx "$temp_dir"
cp -a tex/*.sty "$temp_dir"
cp -a doc/*.pdf "$temp_dir"
cp -a README CHANGELOG SUMMARY "$temp_dir"
cd "$temp_dir"

# shellcheck disable=SC2035
if ctanify --notds totalcount.ins totalcount.dtx README "CHANGELOG=doc/latex/totalcount" "SUMMARY=doc/latex/totalcount" *.pdf
then
  cp -a "$temp_dir/totalcount.tar.gz" "$dist_dir/totalcount_$(date --rfc-3339=date).tar.gz"
  cd "$dist_dir"

#  ctanupload -l -p -u http://www.ctan.org/upload \
#    --contribution=totalcount \
#    --name "Axel Sommerfeldt" --email axel.sommerfeldt@f-m.fm \
#    --summary-file $dist_dir/doc/SUMMARY \
#    --directory=/macros/latex/contrib/totalcount \
#    --DoNotAnnounce=0 \
#    --license=free --freeversion=lppl \
#    --file=$temp_dir/totalcount.tar.gz
fi

